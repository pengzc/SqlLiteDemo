package com.example.sqltest;

import java.io.File;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/* 
 * @author lingdududu 
 */
public class SQLiteActivity extends Activity {
	private static final String TAG = "TestSQLite";
	/** Called when the activity is first created. */
	// 声明各个按钮
	private Button createBtn;
	private Button insertBtn;
	private Button updateBtn;
	private Button queryBtn;
	private Button deleteBtn;
	private Button ModifyBtn;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// 调用creatView方法
		creatView();
		// setListener方法
		setListener();
	}

	// 通过findViewById获得Button对象的方法
	private void creatView() {
		createBtn = (Button) findViewById(R.id.createDatabase);
//		updateBtn = (Button) findViewById(R.id.updateDatabase);
		insertBtn = (Button) findViewById(R.id.insert);
		ModifyBtn = (Button) findViewById(R.id.update);
		queryBtn = (Button) findViewById(R.id.query);
		deleteBtn = (Button) findViewById(R.id.delete);
	}

	// 为按钮注册监听的方法
	private void setListener() {
		createBtn.setOnClickListener(new CreateListener());
//		updateBtn.setOnClickListener(new UpdateListener());
		insertBtn.setOnClickListener(new InsertListener());
		ModifyBtn.setOnClickListener(new ModifyListener());
		queryBtn.setOnClickListener(new QueryListener());
		deleteBtn.setOnClickListener(new DeleteListener());
	}

	// 创建数据库的方法
	class CreateListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			if (isDataBaseExist()) {
				return;
			}
			// 创建StuDBHelper对象
			DvrImgDBHelper dbHelper = new DvrImgDBHelper(SQLiteActivity.this,
					"dvr_img_db", null, 1);
			// 得到一个可读的SQLiteDatabase对象
			SQLiteDatabase db = dbHelper.getReadableDatabase();
		}
	}

/*	// 更新数据库的方法
	class UpdateListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			// 数据库版本的更新,由原来的1变为2
			DvrImgDBHelper dbHelper = new DvrImgDBHelper(SQLiteActivity.this,
					"dvr_img_db", null, 2);
			SQLiteDatabase db = dbHelper.getReadableDatabase();
		}
	}
*/
	
	/*	id int, 
	file_name varchar(50), 
	take_time datetime, 
	latitude real, 
	longtitude real, 
	upload_state int, 
	from int*/
	
	// 插入数据的方法
	class InsertListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			DvrImgDBHelper dbHelper = new DvrImgDBHelper(SQLiteActivity.this,
					"dvr_img_db", null, 1);
			// 得到一个可写的数据库
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			// 生成ContentValues对象 //key:列名，value:想插入的值
			ContentValues cv = new ContentValues();
			// 往ContentValues对象存放数据，键-值对模式
//			cv.put("id", null);
			cv.put("file_name", "20160808_122112A");
			cv.put("take_time", "2016-09-03 12:25:23");
			cv.put("latitude", 50.10);
			cv.put("longtitude", -104.10);
			cv.put("upload_state", 1);
			cv.put("from_type", 1);
			// 调用insert方法，将数据插入数据库
			db.insert("dvr_img_table", null, cv);
			// 关闭数据库
			db.close();
		}
	}

	// 查询数据的方法
	class QueryListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			Log.i(TAG, "query");
			DvrImgDBHelper dbHelper = new DvrImgDBHelper(SQLiteActivity.this,
					"dvr_img_db", null, 1);
			// 得到一个可写的数据库
			SQLiteDatabase db = dbHelper.getReadableDatabase();
			// 参数1：表名
			// 参数2：要想显示的列
			// 参数3：where子句
			// 参数4：where子句对应的条件值
			// 参数5：分组方式
			// 参数6：having条件
			// 参数7：排序方式
/*			Cursor cursor = db.query("dvr_img_table", new String[] { "id", "file_name",
					"take_time", "latitude", "longtitude", "upload_state", "from_type" }, "id>=0", null, null, null,
					null);*/
			Cursor cursor = db.query("dvr_img_table", new String[] { "*" }, "id>=?", new String[] {"0"}, null, null,null);
			Log.i(TAG,"result ColumnCount " + cursor.getColumnCount());
			Log.i(TAG,"result Count " + cursor.getCount());
			while (cursor.moveToNext()) {
				String id = cursor.getString(cursor.getColumnIndex("id"));
				String file_name = cursor.getString(cursor.getColumnIndex("file_name"));
				String take_time = cursor.getString(cursor.getColumnIndex("take_time"));
				String latitude = cursor.getString(cursor.getColumnIndex("latitude"));
				String longtitude = cursor.getString(cursor.getColumnIndex("longtitude"));
				String upload_state = cursor.getString(cursor.getColumnIndex("upload_state"));
				String from = cursor.getString(cursor.getColumnIndex("from_type"));
				Log.i(TAG, "id " + id + " file_name" + file_name + " take_time " + take_time + " latitude " + latitude + " longtitude " + longtitude + " upload_state " + upload_state + " from_type " + from);
			}
			// 关闭数据库
			db.close();
		}
	}

	// 修改数据的方法
	class ModifyListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			DvrImgDBHelper dbHelper = new DvrImgDBHelper(SQLiteActivity.this,
					"dvr_img_db", null, 1);
			// 得到一个可写的数据库
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			ContentValues cv = new ContentValues();
			cv.put("upload_state", 0);
			// where 子句 "?"是占位符号，对应后面的"1",
			String whereClause = "file_name==?";
			String[] whereArgs = { "20160808_122112A" };
			// 参数1 是要更新的表名
			// 参数2 是一个ContentValeus对象
			// 参数3 是where子句
			db.update("dvr_img_table", cv, whereClause, whereArgs);
		}
	}

	// 删除数据的方法
	class DeleteListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			DvrImgDBHelper dbHelper = new DvrImgDBHelper(SQLiteActivity.this,
					"dvr_img_db", null, 1);
			// 得到一个可写的数据库
			SQLiteDatabase db = dbHelper.getReadableDatabase();
			String whereClauses = "id==?";
			String[] whereArgs = { String.valueOf(100) };
			// 调用delete方法，删除数据
			db.delete("dvr_img_table", whereClauses, whereArgs);
		}
	}
	
	boolean isDataBaseExist() {
		File db = new File("dvr_img_db");
		if (db.exists()) {
			return true;
		} else {
			return false;
		}
	}
}
